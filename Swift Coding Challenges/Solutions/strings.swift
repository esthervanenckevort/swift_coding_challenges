func uniqueCharacters(input: String) -> Bool {
    var set = Set<Character>()
    for char in input {
        if set.contains(char) {
            return false
        }
        set.update(with: char)
    }
    return true
}

func palindrome(input: String) -> Bool {
	let lowercased = input.lowercased()
	return "\(String(lowercased.reversed()))" == lowercased
}

func haveSameCharacters(_ first: String, _ second: String) -> Bool {
	return first.sorted() == second.sorted()
}

extension String {
	func fuzzyContains(_ value: String) -> Bool {
		let upperSelf = self.uppercased()
		let upperValue = value.uppercased()
        if let _ = upperSelf.range(of: upperValue) {
            return true
        }
        return false
	}

    func count(_ character: Character) -> Int {
        return self.reduce(0) { (result, char) -> Int in
            return char == character ? result + 1 : result
        }
    }

    func removeDuplicates() -> String {
        var chars = Set<Character>()
        let result = self.compactMap { chars.contains($0) ? nil : chars.insert($0).1 }
        return String(result)
    }

    func condenseWhiteSpace() -> String {
        return self.replacingOccurrences(of: " +", with: " ", options: .regularExpression)
    }

    func isRotated(_ value: String) -> Bool {
        guard value.count == self.count else { return false }
        return (self + self).contains(value)
    }

    func isPangram() -> Bool {
        let set = Set(self.lowercased())
        let letters = set.filter { $0 >= "a" && $0 <= "z" }
        return letters.count == 26
    }

    func vowels() -> Int {
        let vowels = "aeiou"
        return self.lowercased().filter { vowels.contains($0) }.count
    }

    func consonants() -> Int {
        let consonants = "bcdfghjklmnpqrstvwxyz"
        return self.lowercased().filter { consonants.contains($0) }.count
    }

    func vowelsAndConsonantsCount() -> (vowels: Int, consonants: Int) {
        return (vowels: vowels(), consonants: consonants())
    }

    func differsBy3OrLess(with value: String) -> Bool {
        guard self.count == value.count else {
            return false
        }
        var difference = 0
        var first = Array(self.utf8)
        var second = Array(value.utf8)
        var i = 0
        repeat {
            if first[i] != second[i] {
                difference += 1
            }
            i += 1
        } while i < first.count && difference <= 3
        return difference <= 3
    }

    func longestPrefix() -> String {
        let words = self.split(separator: " ")
        guard let first = words.first else { return "" }
        return words.dropFirst().reduce(String(first)) { return $0.commonPrefix(with: $1)}
    }

    func runLengthEncoded() -> String {
        guard self.count > 0 else {
            return self
        }
        var currentChar: Character?
        var length = 0
        var result = ""
        for char in self {
            if currentChar != char {
                if let currentChar = currentChar {
                    result += "\(currentChar)\(length)"
                }
                currentChar = char
                length = 1
            } else {
                length += 1
            }
        }
        if let currentChar = currentChar {
            result += "\(currentChar)\(length)"
        }
        return result
    }

    private func permutations(for string: String, with prefix: String = "") -> [String] {
        guard string.count > 0 else {
            return [prefix]
        }
        var results = [String]()
        let array = Array(string)
        for (index, value) in array.enumerated() {
            let left = String(array[0 ..< index])
            let right = String(array[index + 1 ..< array.count])
            results.append(contentsOf: permutations(for: left + right, with: prefix + String(value)))
        }
        return results
    }

    func permutations() -> [String] {
        return permutations(for: self)
    }

    func reverseWords() -> String {
        let words = self.split(separator: " ").map { String($0.reversed()) }
        return words.joined(separator: " ")
    }
}
