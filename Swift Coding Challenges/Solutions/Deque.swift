//
//  Deque.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 10-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

struct Deque<Element> {
    private var stack = [Element]()

    mutating func pushBack(_ element: Element) {
        stack.append(element)
    }

    mutating func pushFront(_ element: Element) {
        stack.insert(element, at: 0)
    }

    mutating func popBack() -> Element? {
        return stack.popLast()
    }

    mutating func popFront() -> Element? {
        guard stack.count > 0 else { return nil }
        return stack.removeFirst()
    }

    var count: Int {
        return stack.count
    }
}
