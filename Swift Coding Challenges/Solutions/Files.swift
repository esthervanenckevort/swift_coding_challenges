//
//  Files.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 08-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation
import os

func getLastLines(_ lines: Int, of file: URL) -> String? {
    guard lines > 0 else {
        return nil
    }
    guard file.isFileURL else {
        return nil
    }
    guard let data = try? String(contentsOf: file) else {
        return nil
    }
    let array = data.components(separatedBy: .newlines).filter { $0.count > 0 }
    guard array.count > 0 else {
        return nil
    }
    let start = max(0, array.count - lines)
    return array[start..<array.count].reversed().joined(separator: ", ")
}

func getFiles(in directory: URL, after date: Date) -> [URL] {
    let fileManager = FileManager.default
    guard directory.isFileURL else {
        return []
    }
    guard let files = try? fileManager.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) else {
        return []
    }
    return files.filter {
        guard let attributes = try? fileManager.attributesOfItem(atPath: $0.path) else {
            return false
        }
        guard $0.pathExtension == "jpeg" || $0.pathExtension == "jpg" else {
            return false
        }
        guard let creationDate = attributes[.creationDate] as? Date else {
            return false
        }
        return creationDate > date
    }
}

extension URL {
    func copy(to directory: URL) -> Bool {
        let fileManager = FileManager.default
        var isDirectory: ObjCBool = false
        fileManager.fileExists(atPath: self.path, isDirectory: &isDirectory)
        guard directory.isFileURL == true,
            isDirectory.boolValue == true
        else {
            return false
        }
        do {
            try fileManager.copyItem(at: self, to: directory)
        } catch {
            os_log("%@", error.localizedDescription)
            return false
        }
        return true
    }
}

func getWordFrequencies(for file: URL) -> [String:Int] {
    guard let file = try? String(contentsOf: file)
        else { return [:] }
    var separators = CharacterSet.whitespacesAndNewlines
    separators.formUnion(.punctuationCharacters)
    let words = file.components(separatedBy: separators).lazy.filter { $0.count > 0 }.map { ($0, 1) }
    return Dictionary(words) { $0 + $1 }
}
