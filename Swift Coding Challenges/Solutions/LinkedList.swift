//
//  LinkedList.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 10-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

struct LinkedList<Element>: MutableCollection, CustomDebugStringConvertible, CustomStringConvertible {
    var debugDescription: String {
        return "values: \(description) count: \(count) startIndex: \(startIndex) endIndex: \(endIndex) node: \(String(describing: node))"
    }

    var description: String {
        var elements = [String]()
        for element in self {
            elements.append(String(describing: element))
        }
        return "[\(elements.joined(separator: ", "))]"
    }

    subscript(position: Int) -> Element {
        get {
            var node = self.node
            for _ in startIndex..<position {
                node = node?.next
            }
            return node!.value
        }
        set(newValue) {
            var node = self.node
            for _ in startIndex..<position {
                node = node?.next
            }
            node?.value = newValue
        }
    }

    func index(after i: Int) -> Int {
        return i + 1
    }

    typealias Iterator = NodeIterator<Element>
    typealias Index = Int

    var startIndex: Index {
        return node != nil ? 0 : -1
    }

    var endIndex: Index {
        return count - 1
    }

    class NodeIterator<Element>: IteratorProtocol {
        private var node: Node<Element>?
        func next() -> Element? {
            defer {
                node = node?.next
            }
            return node?.value
        }
        init(node: Node<Element>?) {
            self.node = node
        }
    }

    func makeIterator() -> Iterator {
        return NodeIterator<Element>(node: node)
    }

    class Node<Element> {
        var value: Element
        var next: Node<Element>?

        init(with element: Element) {
            value = element
        }
    }

    private var node: Node<Element>?
    private(set) var count: Int

    init<S: Collection>(with collection: S) where S.Element == Element {
        var previous: Node<Element>?
        for value in collection {
            let newNode = Node(with: value)
            if let predecessor = previous {
                predecessor.next = newNode
            } else {
                node = newNode
            }
            previous = newNode
        }
        count = collection.count
    }

    mutating func append(_ element: Element) {
        if node == nil {
            node = Node(with: element)
        } else {
            var lastNode = node
            while lastNode?.next != nil {
                lastNode = lastNode?.next
            }
            lastNode?.next = Node(with: element)
        }
        count += 1
    }

    private func seek(to index: Int) -> (node: Node<Element>?, previous: Node<Element>?) {
        guard index <= endIndex else { return (node: nil, previous: nil) }
        guard index > 0 else { return (node: node, previous: nil) }
        var pos = 0
        var targetNode = node
        var lastNode: Node<Element>?
        while pos < index && lastNode != nil {
            pos += 1
            lastNode = targetNode
            targetNode = targetNode?.next
        }
        assert(pos == index)
        return (node: targetNode, previous: lastNode)
    }

    mutating func insert(_ element: Element, at position: Int = 0) {
        let (current, previous) = seek(to: position)
        let newNode = Node(with: element)
        if previous != nil {
            previous?.next = newNode
        } else {
            node = newNode
        }
        newNode.next = current
        count += 1
    }

    mutating func remove(at index: Int) {
        guard index <= endIndex else { return }
        let (targetNode, lastNode) = seek(to: index)
        if let targetNode = targetNode {
            if lastNode == nil {
                node = targetNode.next
            } else {
                lastNode?.next = targetNode.next
            }
            count -= 1
        }
    }

    func nodeAtMidpoint() -> Element? {
        guard count > 0 else { return nil }
        return self[count / 2]
    }

    func reversed() -> LinkedList<Element> {
        var result = LinkedList<Element>(with: [])
        for element in self {
            result.insert(element)
        }
        return result
    }

    mutating func reverse() {
        self = reversed()
    }
}
