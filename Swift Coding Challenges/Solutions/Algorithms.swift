//
//  Algorithms.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 15-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

func isIsomorphic(_ lhs: Any, _ rhs: Any) -> Bool {
    let lhs = String(describing: lhs)
    let rhs = String(describing: rhs)
    guard lhs.count == rhs.count else { return false }
    var mapping = [Character:Character]()
    var lhsIterator = lhs.makeIterator()
    var rhsIterator = rhs.makeIterator()
    while let lhsValue = lhsIterator.next(), let rhsValue = rhsIterator.next() {
        if let value = mapping[lhsValue] {
            guard rhsValue == value else { return false }
        } else {
            if mapping.values.contains(rhsValue) { return false }
            mapping[lhsValue] = rhsValue
        }
    }
    return true
}

func isBalanced(_ string: String) -> Bool {
    var iterator = string.makeIterator()
    var stack = [Character]()
    let pairs: [Character:Character] = ["[": "]", "(": ")", "{": "}", "<": ">"]
    while let value = iterator.next() {
        if pairs.keys.contains(value) {
            stack.append(value)
        } else if let opening = stack.popLast() {
            guard pairs[opening] == value else { return false }
        } else {
            return false
        }
    }
    return stack.count == 0
}

extension Array where Element: Comparable {

    private func mergesort(_ array: [Element]) -> [Element] {
        guard array.count > 1 else {
            return array
        }
        let midpoint = array.count / 2
        let left = Array(array[array.startIndex ..< midpoint])
        let right = Array(array[midpoint...])
        var lhsIterator = mergesort(left).makeIterator()
        var rhsIterator = mergesort(right).makeIterator()
        var result = [Element]()
        result.reserveCapacity(array.count)
        var lhs = lhsIterator.next()
        var rhs = rhsIterator.next()
        while let lhsValue = lhs, let rhsValue = rhs {
            if lhsValue < rhsValue {
                result.append(lhsValue)
                lhs = lhsIterator.next()
            } else {
                result.append(rhsValue)
                rhs = rhsIterator.next()
            }
        }
        while let value = lhs {
            result.append(value)
            lhs = lhsIterator.next()
        }
        while let value = rhs {
            result.append(value)
            rhs = rhsIterator.next()
        }
        return result
    }

    func quicksorted() -> [Element] {
        guard count > 1 else { return self }
        let pivot = self[Int.random(in: 0..<count)]
        var smaller = [Element]()
        smaller.reserveCapacity(count / 2)
        var equal = [Element]()
        equal.reserveCapacity(count / 2)
        var larger = [Element]()
        larger.reserveCapacity(count / 2)
        for element in self {
            if element < pivot {
                smaller.append(element)
            } else if element > pivot {
                larger.append(element)
            } else {
                equal.append(element)
            }
        }
        return smaller.quicksorted() + equal + larger.quicksorted()
    }
}

func hasWinner(board: [[Character]]) -> Bool {
    precondition(board.count == 3)
    precondition(board[0].count == 3)
    precondition(board[1].count == 3)
    precondition(board[2].count == 3)

    // Horizontal
    for x in 0...2 {
        if board[x][0] == board[x][1] && board[x][0] == board[0][2]
            && (board[x][0] == "X" || board[x][0] == "O") {
            return true
        }
    }

    // Vertical
    for y in 0...2 {
        if board[0][y] == board[1][y] && board[0][y] == board[2][y]
            && (board[0][y] == "X" || board[0][y] == "O"){
            return true
        }
    }

    // Diagonal
    if board[0][0] == board[1][1] && board[0][0] == board[2][2]
        && (board[0][0] == "X" || board[0][0] == "O"){
        return true
    }
    return board[0][2] == board[1][1] && board[2][0] == board[1][1]
        && (board[1][1] == "X" || board[1][1] == "O")
}

func primes(upTo max: Int) -> [Int] {
    guard max > 2 else { return [] }
    var numbers = Array(stride(from: 3, to: max, by: 2))
    var index = numbers.startIndex
    while index < numbers.endIndex {
        let prime = numbers[index]
        if prime > -1  {
            var swipeIndex = numbers.index(index, offsetBy: prime)
            while swipeIndex < numbers.endIndex {
                numbers[swipeIndex] = -1
                swipeIndex = numbers.index(swipeIndex, offsetBy: prime)
            }
        }
        index = numbers.index(after: index)
    }
    return [2] + numbers.compactMap { $0 != -1 ? $0 : nil }
}

func primesPaul(upTo max: Int) -> [Int] {
    guard max > 1 else { return [] }
    var sieve = [Bool](repeating: true, count: max)
    sieve[0] = false
    sieve[1] = false
    for number in 2 ..< max {
        if sieve[number] == true {
            for multiple in stride(from: number * number, to:
                sieve.count, by: number) {
                    sieve[multiple] = false
            }
        } }
    return sieve.enumerated().compactMap { $1 == true ? $0 :
        nil }
}

func degrees(from points: (first: CGPoint, second: CGPoint)) -> Double {
    let opposite = points.first.y - points.second.y
    let adjectant = points.first.x - points.second.x
    let angle = Measurement(value: Double(atan2(opposite, adjectant)), unit: UnitAngle.radians)
    return angle.converted(to: .degrees).value - 90
}

func floodFill(board: [[Int]], startingAt point: (x: Int, y: Int), with newValue: Int) -> [[Int]] {

    func replaceNeighbours(board: inout [[Int]], startingAt point: (x: Int, y: Int), oldValue: Int, with newValue: Int ) {
        for (x, y) in [(point.x - 1, point.y), (point.x + 1, point.y), (point.x, point.y - 1), (point.x, point.y + 1)] {
            guard (0..<10).contains(x), (0..<10).contains(y) else { continue }
            guard board[x][y] == oldValue else { continue }
            board[x][y] = newValue
            replaceNeighbours(board: &board, startingAt: (x, y), oldValue: oldValue, with: newValue)
        }
    }
    let oldValue = board[point.x][point.y]
    var newBoard = board
    newBoard[point.x][point.y] = newValue
    replaceNeighbours(board: &newBoard, startingAt: point, oldValue: oldValue, with: newValue)
    return newBoard
}

func queens(board: [Int], queen: Int) -> Int {
    func delta(_ first: Int, _ second: Int) -> Int {
        return abs(first - second)
    }
    func emptySquare(_ row: Int, _ column: Int) -> String {
        return (row % 2) == (column % 2) ? "◻︎" : "◼︎"
    }
    func canPlay(row: Int, column: Int) -> Bool {
        for otherRow in 0 ..< row {
            let otherColumn = board[otherRow]
            guard otherColumn != column else { return false }
            guard delta(row, otherRow) != delta(column, otherColumn) else { return false }
        }
        return true
    }
    func display(board: [Int]) {
        for row in 0 ..< board.count {
            for column in 0 ..< board.count {
                if column == board[row] {
                    print("♕", terminator: "")
                } else {
                    print(emptySquare(row, column), terminator: "")
                }
            }
            print()
        }
        print()
    }
    guard queen < board.count else {
        display(board: board)
        return 1
    }
    var results = 0
    var board = board
    let row = queen
    for column in 0 ..< board.count {
        if canPlay(row: row, column: column) {
            board[row] = column
            results += queens(board: board, queen: queen + 1)
        }
    }
    return results
}
