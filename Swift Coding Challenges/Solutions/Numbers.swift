//
//  Numbers.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 08-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

func random(min: Int, max: Int) -> Int {
    return Int(arc4random_uniform(UInt32(max - min + 1))) + min
}

func powerOf(value: Int, power: Int) -> Int {
    var result = 1
    for _ in 1...power {
        result *= value
    }
    return result
}

extension Int {
    func isPrime() -> Bool {
        guard self > 2 else {
            return self > 1 ? self % 2 == 0 : false
        }
        guard self % 2 != 0 else {
            return false
        }
        var divisor = 3
        repeat {
            if self % divisor == 0 {
                return false
            }
            divisor += 2
        } while Double(divisor) < sqrt(Double(self))
        return true
    }

    func binaryOnes() -> (previous: Int?, next: Int?) {
        guard self != 0 else {
            return (previous: nil, next: nil)
        }
        let stringRep = String(self, radix: 2)
        let ones = stringRep.count("1")
        var next = self
        repeat {
            next = next.unsafeAdding(1)
        } while next > self && String(next, radix: 2).count("1") != ones
        var prev = self
        repeat {
            prev = prev.unsafeAdding(-1)
        } while prev < self && String(prev, radix: 2).count("1") != ones
        return (previous: prev < self ? prev : nil,
                next: next > self ? next : nil)
    }

    func squareRoot() -> Int {
        guard self > 1 else {
            return 1
        }
        var lowerbound = 0
        var upperbound = 1 + self / 2
        while lowerbound + 1 < upperbound {
            let midpoint = lowerbound + ((upperbound - lowerbound) / 2)
            switch midpoint * midpoint {
            case self: return midpoint
            case ..<self: lowerbound = midpoint
            default: upperbound = midpoint
            }
        }
        return lowerbound
    }

    func subtract(value: Int) -> Int {
        return -1 * value + self
    }
}

extension UInt8 {
    func binaryReverse() -> Int {
        var string = String(String(self, radix: 2).reversed())
        while string.count < 8 {
            string.append("0")
        }
        return Int(string, radix: 2)!
    }
}

extension String {
    func isNumeric() -> Bool {
        let numeric = "0123456789"
        return self.filter { numeric.contains($0) }.count == self.count
    }

    func addNumbers() -> Int {
        let numbers = String(self.map { Int(String($0)) != nil ? $0 : " " }).split(separator: " ")
        return numbers.reduce(0) { $0 + (Int($1) ?? 0) }
    }

    func addNumbers2() -> Int {
        return self.reduce((result: 0, number: 0)) {
            var current = $0
            if let number = Int(String($1)) {
                current.result -= $0.number
                current.number = $0.number * 10 + number
                current.result += current.number
            } else {
                current.number = 0
            }
            return current
        }.result
    }
}
