//
//  BufferedFileReader.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 09-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

class BufferedFileReader {
    private var buffer: [UInt8]
    private let stream: InputStream
    private static let minimumBufferSize = 4096
    private var index: Int
    private let newLine: NewLine
    private var encoding: String.Encoding

    enum NewLine {
        case unix
        case windows
        case mac
        case qnx
        case acorn
        case ebcdic
        case atari
        case zx80

        func get() -> [UInt8] {
            switch self {
            case .unix: return [10]
            case .windows: return [13, 10]
            case .mac: return [13]
            case .qnx: return [30]
            case .acorn: return [10, 13]
            case .ebcdic: return [21]
            case .atari: return [155]
            case .zx80: return [118]
            }
        }
    }

    init?(fileAtURL url: URL, encoding: String.Encoding = .utf8, newLine: NewLine = .unix, bufferSize size: Int = BufferedFileReader.minimumBufferSize) {
        let capacity = size < BufferedFileReader.minimumBufferSize ? BufferedFileReader.minimumBufferSize : size
        buffer = Array()
        buffer.reserveCapacity(capacity)
        guard let stream = InputStream(url: url) else {
            return nil
        }
        self.stream = stream
        stream.open()
        index = 0
        self.encoding = encoding
        self.newLine = newLine
        fillBuffer()
    }

    private func fillBuffer() {
        if index > 0 {
            let end = index < buffer.count ? index : buffer.count
            buffer.removeSubrange(0..<end)
        }
        var newBuffer = Array<UInt8>(repeating: 0, count: buffer.capacity - buffer.count)
        let bytesRead = stream.read(&newBuffer, maxLength: newBuffer.count)
        if bytesRead > 0 {
            buffer.append(contentsOf: newBuffer[0..<bytesRead])
        }
        index = 0
    }

    private func seekRange() -> Range<Int> {
        let bytesForNewLine = newLine.get().count
        let endpos = buffer.count < buffer.capacity ? buffer.count : buffer.count - bytesForNewLine
        return index..<endpos
    }

    func readLine() -> String? {
        var indexOfNewLine: Int?
        var line = [UInt8]()
        let bytesForNewLine = newLine.get().count
        repeat {
            let range = seekRange()
            indexOfNewLine = buffer[range].firstIndex(of: newLine.get()[0])
            for byte in 1..<bytesForNewLine {
                guard var index = indexOfNewLine else {
                    break
                }
                index += 1
                indexOfNewLine = buffer[index] == newLine.get()[byte] ? index : nil
            }
            let indexOfLastByteToCopy = (indexOfNewLine ?? buffer.count) - bytesForNewLine
            if indexOfLastByteToCopy > index {
                line += Array(buffer[index...indexOfLastByteToCopy])
            }
            index = (indexOfNewLine ?? buffer.count - bytesForNewLine) + 1

            if index >= buffer.count - bytesForNewLine {
                fillBuffer()
            }
        } while indexOfNewLine == nil && hasMoreData()

        return String(bytes: line, encoding: encoding)
    }

    func hasMoreData() -> Bool {
        return buffer.count > 0
    }

    deinit {
        stream.close()
    }
}

//let hamlet = URL(fileURLWithPath: "/Users/david/Developer/Swift Coding Challenges/Swift Coding Challenges/hamlet.txt")
//var lines = 0
//if let reader = BufferedFileReader(fileAtURL: hamlet, encoding: .utf8, newLine: .windows) {
//    while reader.hasMoreData() {
//        if let line = reader.readLine() {
//            print(line)
//            lines += 1
//        } else {
//            os_log("Read nil")
//        }
//    }
//}
//print("\(hamlet.lastPathComponent) has \(lines) lines.")
