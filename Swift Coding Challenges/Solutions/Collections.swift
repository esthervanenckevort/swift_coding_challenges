//
//  Arrays.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 09-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation
extension Collection where Element: Numeric, Element: Hashable {
    func scc_sum() -> Element {
        return self.reduce(0, +)
    }

    func sumEvenRepeats() -> Element {
        var dict = [Element: Int]()
        for element in self {
            dict[element, default: 0] += 1
        }
        return dict.filter { $1 % 2 == 0 }.reduce(0) { $0 + $1.key }
    }
}

extension Collection where Element: Comparable {

    func scc_min() -> Element? {
        guard var min = self.first else { return nil }
        for element in self {
            if element < min {
                min = element
            }
        }
        return min
    }

    func smallest(n: Int) -> [Element] {
        return Array(self.sorted().prefix(n))
    }
}

extension Collection where Element: Equatable {
    func scc_index(of element: Element) -> Index? {
        for index in self.indices {
            if self[index] == element {
                return index
            }
        }
        return nil
    }
}

extension Collection where Element == Int {
    func median() -> Double? {
        guard count > 0 else {
            return nil
        }
        var sorted = self.sorted(by: >)
        let middle = count / 2
        if count % 2 == 0 {
            return Double(sorted[middle] + sorted[middle - 1]) / 2
        } else {
            return Double(sorted[middle])
        }
    }
}

extension Collection where Element == Int {
    func missingNumbers(in range: Range<Int>) -> [Element] {
        let missing = Set(range)
        return Array(missing.subtracting(self))
    }

    func count(of number: Character) -> Int {
        return self.reduce(0) { $0 + $1.description.filter { $0 == number }.count }
    }
}

extension Collection {
    func scc_map<T>(_ transform: (Element) throws -> T) rethrows -> [T] {
        var result = Array<T>()
        result.reserveCapacity(self.count)
        for element in self {
            result.append(try transform(element))
        }
        return result
    }
}

func countLargestRange(_ array: [Int]) -> ClosedRange<Int>? {
    guard array.count > 0 else { return nil }
    var bestValue = 0
    var bestRange: ClosedRange<Int>?
    var currentValue = 0
    var start: Int?

    for (index, value) in array.enumerated() {
        if value > 0 {
            currentValue += value
            start = start ?? index
            if currentValue > bestValue {
                bestValue = currentValue
                bestRange = start! ... index
            }
        } else {
            start = nil
            currentValue = 0
        }
    }
    return bestRange
}

extension Array where Element: Comparable {

    func bubblesorted() -> [Element] {
        guard count > 1 else { return self }
        var array = self
        var shouldRerun = true
        var index = 0
        var run = 1
        while shouldRerun {
            if index == (count - run) {
                index = 0
                shouldRerun = false
                run += 1
            }
            while index < (count - run){
                if array[index] > array[index + 1] {
                    array.swapAt(index, index + 1)
                    shouldRerun = true
                }
                index += 1
            }
        }
        return array
    }

    func insertionsorted() -> [Element] {
        guard count > 1 else { return self }
        var sorted = [Element]()
        for element in self {
            var index = 0
            while index < sorted.count && sorted[index] < element {
                index += 1
            }
            sorted.insert(element, at: index)
        }
        return sorted
    }

    mutating func insertionsort() {
        guard count > 1 else { return }
        var index = 0
        while index < count {
            var insertionPoint = count - 1
            while insertionPoint > index && self[insertionPoint] > self[index] {
                insertionPoint -= 1
            }
            if insertionPoint != index {
                let element = self.remove(at: index)
                self.insert(element, at: insertionPoint)
            } else {
                index += 1
            }
        }
    }
}
