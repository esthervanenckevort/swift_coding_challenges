//
//  BinaryTree.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 12-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

struct BinaryTree<Element: Comparable>: CustomDebugStringConvertible {
    class Node {
        var parent: Node?
        var left: Node?
        var right: Node?
        var value: Element

        init(_ value: Element) {
            self.value = value
        }
    }

    var root: Node?

    private mutating func insert(_ node: Node, childOf parent: Node) {
        if node.value > parent.value {
            if let right = parent.right {
                insert(node, childOf: right)
            } else {
                parent.right = node
                node.parent = parent
            }
        } else {
            if let left = parent.left {
                insert(node, childOf: left)
            } else {
                parent.left = node
                node.parent = parent
            }
        }
    }

    mutating func insert(_ value: Element) {
        let node = Node(value)
        guard let parent = root else {
            root = node
            return
        }
        insert(node, childOf: parent )
    }

    private func depth(_ node: Node?, mode f: (Int, Int) -> Int) -> Int {
        guard let node = node else {
            return (-1)
        }
        let left = depth(node.left, mode: f)
        let right = depth(node.right, mode: f)
        return f(left, right) + 1
    }

    func isBalanced() -> Bool {
        guard let node = root else {
            return true
        }
        return depth(node, mode: max) - depth(node, mode: min) <= 1
    }

    init<S: Collection>(_ values: S) where S.Element == Element {
        for value in values {
            insert(value)
        }
    }

    var debugDescription: String {
        guard let first = root else { return "(Empty)" }
        var queue = [Node]()
        queue.append(first)
        var output = ""
        while queue.count > 0 {
            var nodesAtCurrentLevel = queue.count
            while nodesAtCurrentLevel > 0 {
                let node = queue.removeFirst()
                output += "\(node.value) "
                if node.left != nil { queue.append(node.left!) }
                if node.right != nil { queue.append(node.right!) }
                nodesAtCurrentLevel -= 1
            }
            output += "\n"
        }
        return output
    }
}

