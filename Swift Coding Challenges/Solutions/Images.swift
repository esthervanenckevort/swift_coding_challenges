//
//  Images.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 10-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation
import AppKit
import os

extension NSImage {
    static func convertToPNG(url image: URL) -> Bool {
        do {
            let data = try Data(contentsOf: image)
            guard let jpeg = NSBitmapImageRep(data: data) else { return false }
            guard let png = jpeg.representation(using: .png, properties: [:]) else { return false}
            var file = URL(fileURLWithPath: image.path)
            file.deletePathExtension()
            file.appendPathExtension("png")
            try png.write(to: file)
        } catch {
            os_log("Failed to convert image %@ to PNG: %@", [image, error.localizedDescription])
        }
        return true
    }
}
