//
//  main.swift
//  Swift Coding Challenges
//
//  Created by David van Enckevort on 07-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation
import AppKit
import os

print("Chapter 1: Strings")
print("Challenge 1: Are the letters unique?")
assert(uniqueCharacters(input: "No duplicates"), "Failed")
assert(uniqueCharacters(input: "abcdefghijklmnopqrstuvwxyz"), "Failed")
assert(uniqueCharacters(input: "AaBbCc"), "Failed")
assert(uniqueCharacters(input: "Hello, world") == false, "Failed")

print("Challenge 2: Is a string a palindrome?")
assert(palindrome(input: "rotator") == true)
assert(palindrome(input: "Rats live on no evil star") == true)
assert(palindrome(input: "Never odd or even") == false)
assert(palindrome(input: "Hello, world") == false)

print("Challenge 3: Do two strings contain the same characters?")
assert(haveSameCharacters("abca", "abca") == true)
assert(haveSameCharacters("abc", "cba") == true)
assert(haveSameCharacters("a1 b2", "b1 a2") == true)
assert(haveSameCharacters("abc", "abca") == false)
assert(haveSameCharacters("abc", "Abc") == false)
assert(haveSameCharacters("abc", "cbAa") == false)

print("Challenge 4: Does one string contain another?")
assert("Hello, world".fuzzyContains("Hello") == true)
assert("Hello, world".fuzzyContains("WORLD") == true)
assert("Hello, world".fuzzyContains("Goodbye") == false)

print("Challenge 5: Count the characters")
assert("The rain in Spain".count("a") == 2)
assert("Mississippi".count("i") == 4)
assert("Hacking with Swift".count("i") == 3)

print("Challenge 6: Remove duplicate letters from a string")
assert("wombat".removeDuplicates() == "wombat")
assert("hello".removeDuplicates() == "helo")
assert("Mississippi".removeDuplicates() == "Misp")

print("Challenge 7: Condense whitespace")
assert("a   b   c".condenseWhiteSpace() == "a b c")
assert("    a".condenseWhiteSpace() == " a")
assert("abc".condenseWhiteSpace() == "abc")

print("Challenge 8: String is rotated")
assert("abcde".isRotated("eabcd") == true)
assert("abcde".isRotated("cdeab") == true)
assert("abcde".isRotated("abced") == false)
assert("abc".isRotated("a") == false)

print("Challenge 9: Find pangrams")
assert("The quick brown fox jumps over the lazy dog".isPangram() == true)
assert("The quick brown fox jumped over the lazy dog".isPangram() == false)

print("Challenge 10: Vowels and consonants")
assert("Swift Coding Challenges".vowelsAndConsonantsCount() == (vowels: 6, consonants: 15))
assert("Mississippi".vowelsAndConsonantsCount() == (vowels: 4, consonants: 7))

print("Challenge 11: Three different letters")
assert("Clamp".differsBy3OrLess(with: "Cramp") == true)
assert("Clamp".differsBy3OrLess(with: "Crams") == true)
assert("Clamp".differsBy3OrLess(with: "Grams") == true)
assert("Clamp".differsBy3OrLess(with: "Grans") == false)
assert("Clamp".differsBy3OrLess(with: "Clam") == false)
assert("clamp".differsBy3OrLess(with: "maple") == false)

print("Challenge 12: Find longest prefix")
assert("swift switch swill swim".longestPrefix() == "swi")
assert("flip flap flop".longestPrefix() == "fl")

print("Challenge 13: Run-length encoding")
assert("aabbcc".runLengthEncoded() == "a2b2c2")
assert("aaabaaabaaa".runLengthEncoded() == "a3b1a3b1a3")
assert("aaAAaa".runLengthEncoded() == "a2A2a2")

print("Challenge 14: String permutations")
assert("a".permutations() == ["a"])
assert("ab".permutations() == ["ab", "ba"])
assert("abc".permutations() == ["abc", "acb", "bac", "bca", "cab", "cba"])
assert("wombat".permutations().count == 720)

print("Challenge 15: Reverse the words in a string")
assert("Swift Coding Challenges".reverseWords() == "tfiwS gnidoC segnellahC")
assert("The quick brown fox".reverseWords() == "ehT kciuq nworb xof")

print("Chapter 2: Numbers")
print("Challenge 16: Fizz Buzz")
assert(fizzbuzz(number: 1) == "1")
assert(fizzbuzz(number: 2) == "2")
assert(fizzbuzz(number: 3) == "Fizz")
assert(fizzbuzz(number: 4) == "4")
assert(fizzbuzz(number: 5) == "Buzz")
assert(fizzbuzz(number: 15) == "Fizz Buzz")

print("Challenge 17: Generate a random number in a range")
for (min, max) in [(1, 5), (8, 10), (12, 12), (12, 18)] {
    for _ in 0...10 {
        let result = random(min: min, max: max)
        assert(result >= min && result <= max)
    }
}

print("Challenge 18: Recreate the pow() function")
assert(powerOf(value: 4, power: 3) == 64)
assert(powerOf(value: 2, power: 8) == 256)

print("Challenge 19: Swap two numbers")
var a = 1
var b = 2
(a, b) = (b, a)

print("Challenge 20: Number is prime")
assert(11.isPrime() == true)
assert(13.isPrime() == true)
assert(4.isPrime() == false)
assert((-1).isPrime() == false)
assert(0.isPrime() == false)
assert(1.isPrime() == false)
assert(2.isPrime() == true)
assert(9.isPrime() == false)
assert(16777259.isPrime() == true)

print("Challenge 21: Counting binary ones")
assert(12.binaryOnes() == (10, 17))
assert(28.binaryOnes() == (26, 35))
assert(0.binaryOnes() == (nil, nil))

print("Challenge 22: Binary reverse")
assert(UInt8(32).binaryReverse() == 4)
assert(UInt8(4).binaryReverse() == 32)
assert(UInt8(41).binaryReverse() == 148)
assert(UInt8(148).binaryReverse() == 41)

print("Challenge 23: Integer disguised as string")
assert("01010101".isNumeric() == true)
assert("123456789".isNumeric() == true)
assert("9223372036854775808".isNumeric() == true)
assert("1.01".isNumeric() == false)

print("Challenge 24: Add numbers inside a string")
assert("a1b2c3".addNumbers2() == 6)
assert("a10b20c30".addNumbers2() == 60)
assert("h8ers".addNumbers2() == 8)

print("Challenge 25: Calculate a square root by hand")
assert(9.squareRoot() == 3)
assert(16777216.squareRoot() == 4096)
assert(16.squareRoot() == 4)
assert(15.squareRoot() == 3)

print("Challenge 26: Subtract without subtract")
assert(9.subtract(value: 5) == 4)
assert(30.subtract(value: 10) == 20)

print("Chapter 3: Files")
print("Challenge 27: Print last lines")
let titles = URL(fileURLWithPath: "/Users/david/Developer/Swift Coding Challenges/Swift Coding Challenges/titles.txt")
assert(getLastLines(3, of: titles) == "Twelfth Night, Othello, Macbeth")
assert(getLastLines(100, of: titles) == "Twelfth Night, Othello, Macbeth, King Lear, Julius Caesar, Hamlet, Cymbeline, Coriolanus, Antony And Cleopatra")
assert(getLastLines(0, of: titles) == nil)
print("Challenge 28: Log a message")
os_log("%@", log: .default, type: .info, "Challenge 28")

print("Challenge 29: Documents directory")
assert(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first != nil)

print("Challenge 30: New JPEGs")
let formatter = DateFormatter()
formatter.locale = Locale(identifier: "nl_NL")
formatter.dateStyle = .short
formatter.timeStyle = .none
print(getFiles(in: URL(fileURLWithPath: "/Users/david/Pictures"), after: formatter.date(from: "22-12-2017")!))

print("Challenge 31: Copy recursively")
//if let pictures = FileManager.default.urls(for: .downloadsDirectory, in: .userDomainMask).first {
//    assert(pictures.copy(to: FileManager.default.temporaryDirectory.appendingPathComponent("Downloads")) == true)
//    print(FileManager.default.temporaryDirectory.appendingPathComponent("Downloads").path)
//}
let hamlet = URL(fileURLWithPath: "/Users/david/Developer/Swift Coding Challenges/Swift Coding Challenges/hamlet.txt")
print("Challenge 32: Word frequency")
let frequencies = getWordFrequencies(for: hamlet)
print(frequencies.max { $0.value < $1.value } ?? 0)

print("Challenge 35: Convert images")
//let images = getFiles(in: URL(fileURLWithPath: "/Users/david/Developer/Swift Coding Challenges/Swift Coding Challenges/"), after: .distantPast)
//for image in images {
//    _ = NSImage.convertToPNG(url: image)
//}

print("Challenge 36: Print error lines")
let logfile = URL(fileURLWithPath: "/Users/david/Developer/Swift Coding Challenges/Swift Coding Challenges/logfile.txt")
var count = 0
if let reader = BufferedFileReader(fileAtURL: logfile) {
    while reader.hasMoreData() {
        if let line = reader.readLine(), line.hasPrefix("[ERROR]") {
            count += 1
        }
    }
}
assert(count == 54)

print("Collections")
print("Challenge 37: Count the numbers")
assert([5, 15, 55, 515].count(of: "5") == 6)
assert([5, 15, 55, 515].count(of: "1") == 2)
assert([55555].count(of: "5") == 5)
assert([55555].count(of: "1") == 0)

print("Challenge 38: Find N smallest")
assert([1, 2, 3, 4].smallest(n: 3) == [1, 2, 3])
assert(["q", "f", "k"].smallest(n: 10) == ["f", "k", "q"])
assert([256, 16].smallest(n: 3) == [16, 256])
assert([String]().smallest(n: 3) == [])

print("Challenge 39: Sort a string array by length")
assert(["a", "abc", "ab"].sorted { $0.count < $1.count } == ["a", "ab", "abc"])

print("Challenge 40: Missing numbers in array")
var testArray = Array(1...100)
testArray.remove(at: 25)
testArray.remove(at: 20)
testArray.remove(at: 6)
var missing = testArray.missingNumbers(in: Range(1...100))
assert(missing.sorted() == [7, 21, 26])


print("Challenge 41: Find the median")
assert([1, 2, 3].median() == 2)
assert([1, 2, 9].median() == 2)
assert([1, 3, 5, 7, 9].median() == 5)
assert([1, 2, 3, 4].median() == 2.5)
assert([Int]().median() == nil)

print("Challenge 42: Recreate index(of:)")
assert([1, 2, 3].scc_index(of: 1) == 0)
assert([1, 2, 3].scc_index(of: 3) == 2)
assert([1, 2, 3].scc_index(of: 5) == nil)

print("Challenge 43: Linked lists")
let list = LinkedList<Character>(with: "abcdefghijklmnopqrstuvwxyz")
let result = list.reduce("") { $0.count > 0 ? $0 + " " + String($1) : String($1) }
assert(result == "a b c d e f g h i j k l m n o p q r s t u v w x y z")

print("Challenge 44: Linked list mid-point")
let midpoint = list.nodeAtMidpoint()
assert(midpoint == "n")

print("Challenge 46: Recreate map()")
assert([1, 2, 3].scc_map { String($0) } == ["1", "2", "3"])
assert(["1", "2", "3"].scc_map { Int($0)! } == [1, 2, 3])

print("Challenge 47: Recreate min()")
assert([1, 2, 3].scc_min() == 1)
assert(["q", "f", "k"].scc_min() == "f")
assert([4096, 256, 16].scc_min() == 16)
assert([String]().scc_min() == nil)

print("Challenge 48: Implement a deque data structure")
var numbers = Deque<Int>()
numbers.pushBack(5)
numbers.pushBack(8)
numbers.pushBack(3)
assert(numbers.count == 3)
assert(numbers.popFront()! == 5)
assert(numbers.popBack()! == 3)
assert(numbers.popFront()! == 8)
assert(numbers.popBack() == nil)

print("Challenge 49: Sum the even repeats")
assert([1, 2, 2, 3, 3, 4].sumEvenRepeats() == 5)
assert([5, 5, 5, 12, 12].sumEvenRepeats() == 12)
assert([1, 1, 2, 2, 3, 3, 4, 4].sumEvenRepeats() == 10)

print("Challenge 50: Count the largest range")
assert(countLargestRange([0, 1, 1, -1, 2, 3, 1]) == 4...6)
assert(countLargestRange([10, 20, 30, -10, -20, 10, 20]) == 0...2)
assert(countLargestRange([1, -1, 2, -1]) == 2...2)
assert(countLargestRange([2, 0, 2, 0, 2]) == 0...0)
assert(countLargestRange([]) == nil)

print("Challenge 51: Reversing linked lists")
let reversed = list.reversed().reduce("") { $0.count > 0 ? $0 + " " + String($1) : String($1) }
assert(reversed == String("a b c d e f g h i j k l m n o p q r s t u v w x y z".reversed()))

print("Challenge 52: Sum an array of numbers")
assert([1, 2, 3].scc_sum() == 6)
assert([1.0, 2.0, 3.0].scc_sum() == 6.0)
assert(Array<Float>([1.0, 2.0, 3.0]).scc_sum() == 6.0)

print("Challenge 53: Linked lists with a loop")

class LinkedListNode<T>: Hashable {

    var hashValue: Int

    static func == (lhs: LinkedListNode<T>, rhs: LinkedListNode<T>) -> Bool {
        return lhs === rhs
    }

    var value: T
    var next: LinkedListNode<T>?

    init(value: T) {
        self.value = value
        hashValue = UUID().hashValue
    }
}
var start: LinkedListNode<UInt32>?
var previousNode: LinkedListNode<UInt32>?
var linkBackNode: LinkedListNode<UInt32>?
var linkBackPoint = Int(arc4random_uniform(1000))
for i in 1...1000 {
    let node = LinkedListNode(value: arc4random())
    if i == linkBackPoint { linkBackNode = node }
    if let predecessor = previousNode {
        predecessor.next = node
    } else {
        start = node
    }
    previousNode = node
}
previousNode?.next = linkBackNode

var seen = Set<LinkedListNode<UInt32>>()
var node = start
while node != nil {
    if seen.contains(where: { node === $0 }) {
        break
    }
    seen.insert(node!)
    node = node!.next
}
assert(node === linkBackNode)

var tortoise = start
var hare = start
while hare != nil {
    tortoise = tortoise?.next
    hare = hare?.next?.next
    if tortoise == hare {
        break
    }
}
if hare != nil {
    tortoise = start
    while tortoise != hare {
        tortoise = tortoise?.next
        hare = hare?.next
    }
}
assert(hare === linkBackNode)

assert(BinaryTree([2, 1, 3]).isBalanced() == true)
assert(BinaryTree([5, 1, 7, 6, 2, 1, 9]).isBalanced() == true)
assert(BinaryTree([5, 1, 7, 6, 2, 1, 9, 1]).isBalanced() == true)
assert(BinaryTree([5, 1, 7, 6, 2, 1, 9, 1, 3]).isBalanced() == true)
assert(BinaryTree([50, 25, 100, 26, 101, 24, 99]).isBalanced() == true)
assert(BinaryTree(["k", "t", "d", "a", "z", "m", "f"]).isBalanced() == true)
assert(BinaryTree([1]).isBalanced() == true)
assert(BinaryTree([Character]()).isBalanced() == true)

assert(BinaryTree([1, 2, 3, 4, 5]).isBalanced() == false)
assert(BinaryTree([10, 5, 4, 3, 2, 1, 11, 12, 13, 14, 15]).isBalanced() == false)
assert(BinaryTree(["f", "d", "c", "e", "a", "b"]).isBalanced() == false)

print("Algorithms")
print("Challenge 55: Bubble sort")
assert([12, 5, 4, 9, 3, 2, 1].bubblesorted() == [1, 2, 3, 4, 5, 9, 12])
assert(["f", "a", "b"].bubblesorted() == ["a", "b", "f"])
assert([String]().bubblesorted() == [])

print("Challenge 56: Insertion sort")
assert([12, 5, 4, 9, 3, 2, 1].insertionsorted() == [1, 2, 3, 4, 5, 9, 12])
assert(["f", "a", "b"].insertionsorted() == ["a", "b", "f"])
assert([String]().insertionsorted() == [])
var a1: [Int] = [12, 5, 4, 9, 3, 2, 1]
a1.insertionsort()
assert(a1 == [1, 2, 3, 4, 5, 9, 12])
var a2: [String] = ["f", "a", "b"]
a2.insertionsort()
assert(a2 == ["a", "b", "f"])

print("Challenge 57: Isomorphic values")
assert(isIsomorphic("clap", "slap"))
assert(isIsomorphic("rum", "mud"))
assert(isIsomorphic("pip", "did"))
assert(isIsomorphic("carry", "baddy"))
assert(isIsomorphic("cream", "lapse"))
assert(isIsomorphic("123123", "456456"))
assert(isIsomorphic("3.14159", "2.03048"))
assert(isIsomorphic( [1, 2, 1, 2, 3], [4, 5, 4, 5, 6]))

assert(!isIsomorphic("carry", "daddy"))
assert(!isIsomorphic("did", "cad"))
assert(!isIsomorphic("maim", "same"))
assert(!isIsomorphic("curry", "flurry"))
assert(!isIsomorphic("112233", "112211"))

print("Challenge 58: Balanced brackets")
assert(isBalanced("()"))
assert(isBalanced("([])"))
assert(isBalanced("([])(<{}>)"))
assert(isBalanced("([]{}<[{}]>)"))
assert(isBalanced(""))
assert(isBalanced("}{") == false)
assert(isBalanced("([)]") == false)
assert(isBalanced("([)") == false)
assert(isBalanced("([") == false)
assert(isBalanced("[<<<{}>>]") == false)
assert(isBalanced("hello ") == false)

print("Challenge 59: Quicksort")
assert([12, 5, 4, 9, 3, 2, 1].quicksorted() ==  [1, 2, 3, 4, 5, 9, 12])
assert(["f", "a", "b"].quicksorted() == ["a", "b", "f"])
assert([String]() == [])

print("Challenge 60: Tic-Tac-Toe winner")
assert(hasWinner(board: [["X", " ", "O"], [" ", "X", "O"], [" ", " ", "X"]]) == true)
assert(hasWinner(board: [["X", " ", "O"], ["X", " ", "O"], ["X", " ", " "]]) == true)
assert(hasWinner(board: [[" ", "X", " "], ["O", "X", " "], ["O", "X", " "]]) == true)
assert(hasWinner(board: [[" ", "X", " "], ["O", "X", " "], ["O", " ", "X"]]) == false)
assert(hasWinner(board: [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]) == false)

print("Challenge 61: Find prime numbers")
assert(primes(upTo: 10) == [2, 3, 5, 7])
assert(primes(upTo: 11) == [2, 3, 5, 7])
assert(primes(upTo: 12) == [2, 3, 5, 7, 11])
assert(primes(upTo: 100_000) == primesPaul(upTo: 100_000))

print("Challenge 62: Points to angles")
var points = [(first: CGPoint, second: CGPoint)]()
points.append((first: CGPoint.zero, second: CGPoint(x: 0, y: -100)))
points.append((first: CGPoint.zero, second: CGPoint(x: 100, y: -100)))
points.append((first: CGPoint.zero, second: CGPoint(x: 100, y: 0)))
points.append((first: CGPoint.zero, second: CGPoint(x: 100, y: 100)))
points.append((first: CGPoint.zero, second: CGPoint(x: 0, y: 100)))
points.append((first: CGPoint.zero, second: CGPoint(x: -100, y: 100)))
points.append((first: CGPoint.zero, second: CGPoint(x: -100, y: 0)))
points.append((first: CGPoint.zero, second: CGPoint(x: -100, y: -100)))

//assert(points.map { degrees(from: $0) } == [0.0, 45.0, 90.0, 135.0, 180.0, 225.0, 270.0, 315.0])

print("Challenge 63: Flood fill")
import GameplayKit
let random = GKMersenneTwisterRandomSource(seed: 1)
var grid = (1...10).map { _ in (1...10).map { _ in
    Int(random.nextInt(upperBound: 2)) } }
var board = floodFill(board: grid, startingAt: (x: 2, y: 0), with: 5)
assert(board == [[5, 5, 5, 5, 5, 1, 5, 5, 1, 1],
    [5, 1, 1, 5, 5, 5, 5, 1, 0, 0],
    [5, 1, 5, 5, 5, 5, 5, 5, 1, 1],
    [1, 0, 1, 5, 5, 1, 1, 5, 5, 5],
    [1, 0, 1, 5, 1, 1, 1, 1, 1, 5],
    [1, 0, 1, 1, 5, 5, 5, 5, 5, 5],
    [0, 0, 0, 0, 1, 1, 1, 5, 1, 1],
    [1, 1, 1, 0, 0, 1, 1, 1, 1, 1],
    [1, 1, 0, 1, 1, 1, 1, 0, 0, 0],
    [0, 1, 1, 0, 0, 1, 0, 1, 1, 1]])

print("Challenge 64: N Queens")
assert(queens(board: Array(repeating: -1, count: 5), queen: 0) == 10)
assert(queens(board: Array(repeating: -1, count: 8), queen: 0) == 92)
assert(queens(board: Array(repeating: -1, count: 10), queen: 0) == 724)
